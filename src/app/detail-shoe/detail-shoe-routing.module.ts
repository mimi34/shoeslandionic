import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailShoePage } from './detail-shoe.page';

const routes: Routes = [
  {
    path: '',
    component: DetailShoePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailShoePageRoutingModule {}
