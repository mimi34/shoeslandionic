import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailShoePageRoutingModule } from './detail-shoe-routing.module';

import { DetailShoePage } from './detail-shoe.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailShoePageRoutingModule
  ],
  declarations: [DetailShoePage]
})
export class DetailShoePageModule {}
