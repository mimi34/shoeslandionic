// import { TestBed } from '@angular/core/testing';

// import { PhotoService } from './photo.service';

// describe('PhotoService', () => {
//   let service: PhotoService;

//   beforeEach(() => {
//     TestBed.configureTestingModule({});
//     service = TestBed.inject(PhotoService);
//   });

//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });
// });

import { Injectable } from '@angular/core';
import { from } from 'rxjs';

/**
 * camera acces camera
 * source source de la camera
 */
import { Camera, CameraResultType, CameraSource, Photo} from '@capacitor/camera';
import { Filesystem, Directory} from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { UserPhoto } from '../interfaces/UserPhoto';


@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  public photos:UserPhoto[]=[];
   
  private PHOTO_STORAGE: string ='photos';
  

  constructor() { }

  public async takePhoto() {

    const photoTaken = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
    const savedImageFile = await this.savePicture(photoTaken);

    this.photos.unshift({
      filepath: savedImageFile.filepath,
      webviewPath: photoTaken.webPath
    });

    // creation d'un objet cle valeur
      Preferences.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos),
    });
     }

  private async savePicture(photo: Photo) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);
  
    // Write the file to the data directory
    // creation d'un nom de fichier dynamique
    const fileName = new Date().getTime() + '.jpeg';
    // creeation fichier avec le nom crée au dessus et les données
    const savedFile = await Filesystem.writeFile({
      
      path: fileName,/**nom du fichier */
      data: base64Data,/**données en base64 */
      directory: Directory.Data
    });
  
    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: photo.webPath
    };


  }

  private async readAsBase64(photo: Photo) {
    // Fetch the photo, read as a blob, then convert to base64 format
    /**
     * recuperation webPath (image)
     */
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();
    /**
     * appelle la methode convertBlobToBase64
     */
  
    return await this.convertBlobToBase64(blob) as string;
  }
  
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    // instancie nuvel objet FileReader
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    // renvoi des données(photo) en binaire
    reader.readAsDataURL(blob);
  });

  public async loadSaved() {
    // Retrieve cached photo array data
    const photoList = await Preferences.get({ key: this.PHOTO_STORAGE });
    this.photos = JSON.parse(photoList.value) || [];
   // Display the photo by reading into base64 format
for (let photo of this.photos) {
  // Read each saved photo's data from the Filesystem
  const readFile = await Filesystem.readFile({
    path: photo.filepath,
    directory: Directory.Data,
  });

  // Web platform only: Load the photo as base64 data
  photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`;
} 
    
  }

}
