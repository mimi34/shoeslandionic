/**module et composant dont on va avoir besoin 
 * Les Imports : les imports sont les modules qui vont exporter les classes dont nous aurons besoins. 
 * Les components qui utilisent ce module pour
fonctionner. */
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { Shoe } from "../models/Shoe";
import { UserPhoto } from '../interfaces/UserPhoto';

/**decorareur qui permet d'utiliserla classe ailleur*/
@Injectable({
    providedIn: 'root'
})
/**
 * Cette classe servira a envoyer des requetes à l'API
 */
export class ShoesService{

    url='http://localhost:8000';

    /**Variable qui permet de passer des données d'une page à l'autre (tab1 vers tab3) */
    oneShoe : any;
    
    constructor(private http: HttpClient){
        this.http = http; 
    }
    
    /**methode qui envoie la requete dans L'api qui recupere toutes les chaussures */
    getAllShoes() {
        return this.http.get('http://localhost:8000/shoes/readShoes');
    }
    
    /**methode qui envoie la requete dans L'api qui recupere une seule chaussure 
     * avec un parametre id
    */
    getOneShoe(id : any) {
        // this.oneShoe = id;
        return this.http.get('http://localhost:8000/shoes/getOneShoe/' + id);
    }

    /**methode qui envoie la requete dans L'api qui supprime une chaussure
     * avec un parametre id
     */
    delete(id: any) {
        return this.http.delete('http://localhost:8000/shoes/deleteShoe/' + id);
    }
    
    /**methode qui envoie la requete dans L'api qui crée une chaussure 
     * avec un parametre data(données du formulaire)
    */
    postData(data: any) {
        return this.http.post(this.url + '/shoes/createShoes/', data);
        
    }

    /** methode qui envoie la requete dans L'api qui modifie une chaussure 
     * avec un parametre id et data(données du formulaire)
    */
    updateShoe(id: any,data: any) {
        return this.http.put('http://localhost:8000/shoes/updateShoe/' + id, data);
    }
   
}

