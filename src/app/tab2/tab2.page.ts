import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoesService } from "../services/shoes.service";
import { Shoe } from "../models/Shoe";
import { Router } from '@angular/router';
import { PhotoService } from "../services/photo.service";
// import { UserPhoto } from '../interfaces/UserPhoto';
// import { Camera, CameraResultType, CameraSource, Photo} from '@capacitor/camera';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page  {
  name : any;
  size : any;
  value: any;
  photo : any;
  
  
  constructor(public shoesService : ShoesService, public router: Router, 
    public route: Router, public routerModule: Router, 
    public photoService : PhotoService ) {
      
    }
    
    addPhotoGalery() {
      this.photoService.takePhoto();
      console.log("ph "+this.photoService.takePhoto());
      
    }
    
    async ngOnInit() {
      await this.photoService.loadSaved();
      console.log("ph2"+ this.photoService.loadSaved());
      
    }
    
    /**Methode pour créér une chaussure 
     * on rècupére les données du formulaire que l'on met dans la constante 'body'.
     * On envoie ses données avec la mèthode postData */
    createShoe(value: any){
      console.log(value)
      const body = {
        name: this.name,
        size: this.size,
        photo: this.photoService.nomPhoto,
      }
     this.shoesService.postData(body)
     .subscribe(response => {
       console.log(response)
       /** redirection */
       this.router.navigate(['/tabs/tab1']);
     })
  }

  // ngOnDestroy() {
  //   this.name = null;
  //   this.size = null;
  //   this.photo= null;
  //   this.value= null;

  // }

}
