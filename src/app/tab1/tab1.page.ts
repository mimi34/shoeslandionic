import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoesService } from '../services/shoes.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit, OnDestroy {
  result: any;
  resultOne: any;
 
  constructor(public shoesService: ShoesService, public router: Router) {

  }

  /**methode qui à l'initialisation renvoie un tableau d'objet qui contient
   *  toutes les chaussures et leurs caracteristiques*/
  ngOnInit() {
    this.readAllShoes();
  }

  
  /**
   * methode qui affiche toute les chaussures
   * subscribe sert à stocker la rèponse dans une variable
   *  et attendre la reponse
   */
  readAllShoes() {
    this.shoesService.getAllShoes().subscribe((res) => {
      // console.log(res);
      this.result = res;
    });
  }

  /**
   * methode qui recupere le detail d'une chaussure et renvoie un objet d'objet
   * oneShoe variable inialiser dans le service
   */
  detailOneShoe(idShoe: any){
    this.shoesService.oneShoe = idShoe
    console.log("oneShoe " + this.shoesService.oneShoe);
    this.shoesService.getOneShoe(idShoe).subscribe((resOne) => {
      console.log(idShoe);
      console.log(resOne);  
      this.router.navigate(['/detail-shoe']) 
        
    })
  }

  /**
   * methode qui supprime une chaussure
   */
  delete(id: any) {
    this.shoesService.delete(id).subscribe((resDel) => {
      // console.log(resDel);
      this.ngOnInit();
    });
  }

  ngOnDestroy() {
    this.result = null;
    console.log( "22 "+this.result);
    
    // this.resultOne = null;
  }
}
