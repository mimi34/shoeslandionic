export class Shoe {
    _id : String;
    name : String;
    size : Number;

    constructor(_id: String, name: String, size: Number){
        this._id = _id;
        this.name = name;
        this.size = size;
    }
}