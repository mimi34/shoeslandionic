import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoesService } from "../services/shoes.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-detail-shoe',
  templateUrl: './detail-shoe.page.html',
  styleUrls: ['./detail-shoe.page.scss'],
})
export class DetailShoePage implements OnInit {

  
    resultOne : any;
    id : any;
    shoeRecup: any;
    name: any;
    size: any;
    photo: any;
    
    constructor(public shoesService : ShoesService, public router : Router) {}
  
    ngOnInit() {
      this.getOneShoe();
    }
    
    /**
     * methode qui affiche une chaussure avec la variable oneShoe qui est dèfinie dans les services
     */
    getOneShoe(){
      console.log("3 "+ this.shoesService.oneShoe);
    this.shoesService.getOneShoe(this.shoesService.oneShoe).subscribe(res=>{
     this.shoeRecup = res;
     console.log("33 "+this.shoeRecup);
     
    })
   }
  
    updateShoe(id:any){
      let body = {
        name: this.name,
        size: this.size,
        photo: this.photo,
        _id: this.id,
    }
    console.log(id);
    this.shoesService.updateShoe(this.shoesService.oneShoe, body)
    .subscribe(response => {
      console.log(response)
      this.router.navigate(['/tabs/tab1']);
    })
    }
  
    // ngOnDestroy()  {
    //   this.resultOne  = null;
    //   this.id  = null;
    //   this.shoeRecup = null;
    //   this.name = null;
    //   this.size = null;
    //   this.photo = null;
    // }
  
  }
  
